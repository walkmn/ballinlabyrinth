package it.devchallange.ballinlabyrinth;

import android.app.Application;

import com.orhanobut.hawk.Hawk;

public class BallInLabyrinthApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        // good library for quick storage data. (without database, only shared preferences)
        Hawk.init(getApplicationContext()).build();
    }
}
