package it.devchallange.ballinlabyrinth.controller;

import android.os.AsyncTask;

import com.orhanobut.hawk.Hawk;

import java.util.Random;

import it.devchallange.ballinlabyrinth.model.Direction;
import it.devchallange.ballinlabyrinth.model.LabyrinthPoint;
import it.devchallange.ballinlabyrinth.model.GameDataModel;
import it.devchallange.ballinlabyrinth.model.GameMode;

/**
 * Generate, save and load game (GameDataModel).
 */
public class GameBuilder {

    private GameBuilderListener gameBuilderListener;

    public GameBuilder(GameBuilderListener gameBuilderListener) {
        this.gameBuilderListener = gameBuilderListener;
    }

    public void newGame(final int sizeX, final int sizeY, final GameMode gameMode) {
        gameBuilderListener.onStartProcess();

        final LabyrinthFieldGenerator labyrinthFieldGenerator = new LabyrinthFieldGenerator(new LabyrinthFieldGenerator.LabyrinthGeneratorListener() {
            @Override
            public void onGenerated(LabyrinthPoint[][] labyrinthPoints) {

                int circleStartPositionX = 0;
                int circleStartPositionY = 0;

                int finishPositionX = labyrinthPoints[0].length - 1;
                int finishPositionY = labyrinthPoints.length - 1;

                if (gameMode == GameMode.RANDOM) {
                    circleStartPositionX = new Random().nextInt(labyrinthPoints[0].length);
                    circleStartPositionY = new Random().nextInt(labyrinthPoints.length);

                    finishPositionX = new Random().nextInt(labyrinthPoints[0].length);
                    finishPositionY = new Random().nextInt(labyrinthPoints.length);

                    if (circleStartPositionX == finishPositionX && circleStartPositionY == finishPositionY) {
                        // lol
                        if (finishPositionX == 0) {
                            finishPositionX++;
                        } else {
                            finishPositionX--;
                        }
                    }
                }

                gameBuilderListener.onGameReady(
                        new GameDataModel(labyrinthPoints)
                                .setCirclePosition(circleStartPositionX, circleStartPositionY)
                                .setFinishPosition(finishPositionX, finishPositionY)
                );
            }
        }, sizeX, sizeY);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                labyrinthFieldGenerator.generate();
                return null;
            }
        }.execute();
    }

    public void save(GameDataModel gameDataModel) {
        Hawk.put("game_data_model", gameDataModel);
        LabyrinthPoint[][] labyrinthPoints = gameDataModel.getLabyrinthPoints();

        for (int row = 0; row < labyrinthPoints.length; row++) {
            for (int col = 0; col < labyrinthPoints[row].length; col++) {
                LabyrinthPoint labyrinthPoint = labyrinthPoints[row][col];
            }
        }
    }

    public void load() {
        GameDataModel gameDataModel = Hawk.get("game_data_model");
        if (gameDataModel == null) {
            return;
        }
        LabyrinthPoint[][] labyrinthPoints = gameDataModel.getLabyrinthPoints();

        for (int row = 0; row < labyrinthPoints.length; row++) {
            for (int col = 0; col < labyrinthPoints[row].length; col++) {
                LabyrinthPoint labyrinthPoint = labyrinthPoints[row][col];

                if (labyrinthPoint.isHasConnection(Direction.LEFT)) {
                    labyrinthPoint.connectPoint( labyrinthPoints[row][col - 1] );
                }
                if (labyrinthPoint.isHasConnection(Direction.RIGHT)) {
                    labyrinthPoint.connectPoint( labyrinthPoints[row][col + 1] );
                }
                if (labyrinthPoint.isHasConnection(Direction.UP)) {
                    labyrinthPoint.connectPoint( labyrinthPoints[row - 1][col] );
                }
                if (labyrinthPoint.isHasConnection(Direction.DOWN)) {
                    labyrinthPoint.connectPoint( labyrinthPoints[row + 1][col] );
                }
            }
        }
        gameBuilderListener.onGameReady(gameDataModel);
    }

    public interface GameBuilderListener {
        void onStartProcess();
        void onGameReady(GameDataModel gameDataModel);
    }
}
