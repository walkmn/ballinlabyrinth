package it.devchallange.ballinlabyrinth.controller;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Random;

import it.devchallange.ballinlabyrinth.model.Direction;
import it.devchallange.ballinlabyrinth.model.LabyrinthPoint;

/**
 * Generate and connect all labyrinth points
 */
class LabyrinthFieldGenerator {

    private int weight = 3;
    private int height = 3;

    // our logic field
    private LabyrinthPoint[][] labyrinthPoints = new LabyrinthPoint[weight][height];

    private LabyrinthGeneratorListener labyrinthGeneratorListener;

    /**
     *
     * @param weight Number of dots vertically
     * @param height Number of dots horizontally
     */
    LabyrinthFieldGenerator(LabyrinthGeneratorListener labyrinthGeneratorListener, int weight, int height) {
        this.labyrinthGeneratorListener = labyrinthGeneratorListener;
        this.weight = weight;
        this.height = height;

        this.labyrinthPoints = new LabyrinthPoint[weight][height];

    }

    /**
     * Generate labyrinth. (only logic)
     *
     */
    void generate() {

        // init, first blood ...
        for (int row = 0; row < labyrinthPoints.length; row++) {
            for (int col = 0; col < labyrinthPoints[row].length; col++) {
                labyrinthPoints[row][col] = new LabyrinthPoint(row, col);
            }
        }

        LabyrinthPoint currentFreePoint = getFreePoint();

        // 1. Connect all fre points. Use random method.
        while (currentFreePoint != null) {

            LabyrinthPoint nextPoint = randomizeNextPoint(currentFreePoint);

            connectPoints(currentFreePoint, nextPoint);

            currentFreePoint = getFreePoint();
        }

        // 2. Connect point if this one is not in "network"
        for (int row = 0; row < labyrinthPoints.length; row++) {
            for (int col = 0; col < labyrinthPoints[row].length; col++) {

                int nextRow = row + 1;
                int nextCol = col;

                if (nextRow >= labyrinthPoints.length) {
                    nextRow = labyrinthPoints.length - 1;
                    nextCol = col + 1;
                }

                if (nextCol >= labyrinthPoints[row].length) {
                    break;
                }

                LabyrinthPoint point = labyrinthPoints[row][col];
                LabyrinthPoint nextLabyrinthPoint = labyrinthPoints[nextRow][nextCol];

                // check if point in network, if not - connecting.
                // "З будь-якої точки лабіринту можна потрапити в будь-яку іншу"
                if (!point.isHasPointInNetwork(point, nextLabyrinthPoint)) {
                    connectPoints(point, nextLabyrinthPoint);
                }

            }
        }

        labyrinthGeneratorListener.onGenerated(labyrinthPoints);
    }

    private void connectPoints(LabyrinthPoint firstPoint, LabyrinthPoint secondPoint) {
        firstPoint.connectPoint(secondPoint);
        secondPoint.connectPoint(firstPoint);

    }

    @Nullable
    private LabyrinthPoint getFreePoint() {

        for (int row = 0; row < labyrinthPoints.length; row++) {
            for (int col = 0; col < labyrinthPoints[row].length; col++) {

                LabyrinthPoint labyrinthPoint = labyrinthPoints[row][col];

                if (labyrinthPoint.isHasNoConnection()) {
                    return labyrinthPoint;
                }
            }
        }
        return null;
    }

    @NonNull
    // check strength of point, what is the next way of point
    private LabyrinthPoint randomizeNextPoint(LabyrinthPoint currentPoint) {

        int nextDirectionIndex = new Random().nextInt(Direction.values().length);
        Direction nextDirection = Direction.values()[nextDirectionIndex];

        int nextPointX = currentPoint.getX();
        int nextPointY = currentPoint.getY();

        switch (nextDirection) {
            case UP:
                nextPointY--;
                break;

            case LEFT:
                nextPointX--;
                break;

            case RIGHT:
                nextPointX++;
                break;

            case DOWN:
                nextPointY++;
                break;
        }

        // border check
        if ( nextPointX < 0 ) {
            nextPointX = currentPoint.getX() + 1;
        }

        if ( nextPointX >= weight  ) {
            nextPointX = currentPoint.getX() - 1;
        }

        if ( nextPointY < 0 ) {
            nextPointY = currentPoint.getY() + 1;
        }

        if ( nextPointY >= height ) {
            nextPointY = currentPoint.getY() - 1;
        }

        return this.labyrinthPoints[nextPointX][nextPointY];
    }

    interface LabyrinthGeneratorListener {
        void onGenerated(LabyrinthPoint [][] labyrinthPoints);
    }
}
