package it.devchallange.ballinlabyrinth;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import it.devchallange.ballinlabyrinth.controller.GameBuilder;
import it.devchallange.ballinlabyrinth.fragments.GameFragment;
import it.devchallange.ballinlabyrinth.fragments.MenuFragment;
import it.devchallange.ballinlabyrinth.model.GameDataModel;
import it.devchallange.ballinlabyrinth.model.GameMode;

public class MainActivity extends AppCompatActivity implements MenuFragment.GameMenuListener, GameBuilder.GameBuilderListener, GameFragment.GameListener {

    private MenuFragment menuFragment;
    private GameFragment gameFragment;

    private GameBuilder gameBuilder;

    private ProgressDialog loadingDialog;

    private GameDataModel currentGameDataModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menuFragment = new MenuFragment();
        gameFragment = new GameFragment();

        gameBuilder = new GameBuilder(this);

        menuFragment.setGameMenuListener(this);
        gameFragment.setGameListener(this);

        addFragment(menuFragment, false);
    }

    private void addFragment(Fragment fragment, boolean addToBacStack) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (addToBacStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    public void onNewGameClicked() {

        final GameMode[] gameMode = {GameMode.STANDARD};

        // setup spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item);
        adapter.add("Звичайний режим");
        adapter.add("З випадковим входом і виходом");
        LayoutInflater li = LayoutInflater.from(this);
        Spinner dialogView = (Spinner) li.inflate(R.layout.dialog_new_game, null);
        dialogView.setAdapter(adapter);
        dialogView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                gameMode[0] = GameMode.values()[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        new AlertDialog.Builder(this)
                .setTitle("Оберіть розмір лабіринта (сітки), та режим гри:")
                .setView(dialogView)
                .setPositiveButton("2x2", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        gameBuilder.newGame(2, 2, gameMode[0]);
                    }
                })
                //  50х80 - it is hard to look, really), you may to try
                .setNegativeButton("25x30", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        gameBuilder.newGame(25, 30, gameMode[0]);
                    }
                })
                .setNeutralButton("10x10", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        gameBuilder.newGame(10, 10, gameMode[0]);
                    }
                })
                .create()
                .show();
    }

    // menu fragment. (first fragment)
    @Override
    public void onContinueGameClicked() {
        if (currentGameDataModel == null) {
            return;
        }
        showGame(currentGameDataModel);
    }

    @Override
    public void onSaveGameClicked() {
        if (currentGameDataModel == null) {
            return;
        }
        gameBuilder.save(currentGameDataModel);

        Toast.makeText(this, "Гра збережена.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoadGameClicked() {
        gameBuilder.load();
    }

    // game builder
    @Override
    public void onStartProcess() {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this);
            loadingDialog.setMessage("Будь ласка зачекайте...");
        }
        if (!loadingDialog.isShowing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });
        }
    }

    @Override
    public void onGameReady(GameDataModel gameDataModel) {
        if (loadingDialog != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.dismiss();
                }
            });
        }
        showGame(gameDataModel);
    }

    private void showGame(GameDataModel gameDataModel) {
        gameFragment.setGameDataModel(gameDataModel);
        addFragment(gameFragment, true);
    }

    // game process main fragment
    @Override
    public void onGameFinished() {
        new AlertDialog.Builder(this)
                .setTitle("Вітаємо!")
                .setMessage("Ви закінчили гру. Дякую за увагу)")
                .setPositiveButton("Добре", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onBackPressed();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    @Override
    public void onGamePaused(GameDataModel gameDataModel) {
        currentGameDataModel = gameDataModel;
    }
}
