package it.devchallange.ballinlabyrinth.model;

public enum  Direction {
    UP, DOWN, LEFT, RIGHT
}
