package it.devchallange.ballinlabyrinth.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Element of labyrinth - Labyrinth Point.
 *
 * Has information about own position (index) and another connected points.
 */
public class LabyrinthPoint {

    private transient LabyrinthPoint upPoint;
    private transient LabyrinthPoint downPoint;
    private transient LabyrinthPoint leftPoint;
    private transient LabyrinthPoint rightPoint;

    private boolean hasConnectionTop = false;
    private boolean hasConnectionBottom = false;
    private boolean hasConnectionLeft = false;
    private boolean hasConnectionRight = false;

    private int pointX;
    private int pointY;

    public LabyrinthPoint(int x, int y) {
        this.pointX = x;
        this.pointY = y;
    }

    public int getX() {
        return pointX;
    }

    public int getY() {
        return pointY;
    }

    @Override
    public int hashCode() {
        return (pointX * 10) + pointY;
    }

    public void connectPoint(LabyrinthPoint nextPoint) {

        if (nextPoint.equals(this)) {
            throw new RuntimeException("I can not connect myself (point " + nextPoint.toString() + ").");
        }

        if (nextPoint.getY() > getY()) {
            rightPoint = nextPoint;
            hasConnectionRight = true;
            return;
        }

        if (nextPoint.getY() < getY()) {
            leftPoint = nextPoint;
            hasConnectionLeft = true;
            return;
        }

        if (nextPoint.getX() < getX()) {
            upPoint = nextPoint;
            hasConnectionTop = true;
            return;
        }

        if (nextPoint.getX() > getX()) {
            downPoint = nextPoint;
            hasConnectionBottom = true;
        }
    }

    // check if point has in all connected points.
    // recursion
    public boolean isHasPointInNetwork(LabyrinthPoint prevPoint, LabyrinthPoint point) {

        boolean leftDirectionResult = false;
        boolean rightDirectionResult = false;
        boolean topDirectionResult = false;
        boolean bottomDirectionResult = false;

        if (hasConnectionLeft && !leftPoint.equals(prevPoint)) {
            if (leftPoint.equals(point)) {
                return true;
            }
            leftDirectionResult = leftPoint.isHasPointInNetwork(this, point);
        }
        if (leftDirectionResult) {
            return true;
        }

        if (hasConnectionRight && !rightPoint.equals(prevPoint)) {
            if (rightPoint.equals(point)) {
                return true;
            }
            rightDirectionResult = rightPoint.isHasPointInNetwork(this, point);
        }
        if (rightDirectionResult) {
            return true;
        }

        if (hasConnectionTop && !upPoint.equals(prevPoint)) {
            if (upPoint.equals(point)) {
                return true;
            }
            topDirectionResult = upPoint.isHasPointInNetwork(this, point);
        }
        if (topDirectionResult) {
            return true;
        }

        if (hasConnectionBottom && !downPoint.equals(prevPoint)) {
            if (downPoint.equals(point)) {
                return true;
            }
            bottomDirectionResult = downPoint.isHasPointInNetwork(this, point);
        }
        if (bottomDirectionResult) {
            return true;
        }

        return false;
    }

    // get list of points from this point to 'pointToFind'
    // recursion
    public List<LabyrinthPoint> findWayToPoint(LabyrinthPoint prevPoint, LabyrinthPoint pointToFind) {

        List<LabyrinthPoint> findWayPoints = new ArrayList<>();
        findWayPoints.add(this);

        if (hasConnectionLeft && !leftPoint.equals(prevPoint)) {
            if (leftPoint.equals(pointToFind)) {
                findWayPoints.add(pointToFind);
                return findWayPoints;
            }
            List<LabyrinthPoint> leftPoints = leftPoint.findWayToPoint(this, pointToFind);
            if (leftPoints.size() > 1) {
                findWayPoints.addAll(leftPoints);
            }
        }
        if (hasConnectionRight && !rightPoint.equals(prevPoint)) {
            if (rightPoint.equals(pointToFind)) {
                findWayPoints.add(pointToFind);
                return findWayPoints;
            }
            List<LabyrinthPoint> rightPoints = rightPoint.findWayToPoint(this, pointToFind);
            if (rightPoints.size() > 1) {
                findWayPoints.addAll(rightPoints);
            }
        }
        if (hasConnectionTop && !upPoint.equals(prevPoint)) {
            if (upPoint.equals(pointToFind)) {
                findWayPoints.add(pointToFind);
                return findWayPoints;
            }
            List<LabyrinthPoint> upPoints = upPoint.findWayToPoint(this, pointToFind);
            if (upPoints.size() > 1) {
                findWayPoints.addAll(upPoints);
            }
        }
        if (hasConnectionBottom && !downPoint.equals(prevPoint)) {
            if (downPoint.equals(pointToFind)) {
                findWayPoints.add(pointToFind);
                return findWayPoints;
            }
            List<LabyrinthPoint> downPoints = downPoint.findWayToPoint(this, pointToFind);
            if (downPoints.size() > 1) {
                findWayPoints.addAll(downPoints);
            }
        }
        return findWayPoints;
    }

    public boolean isHasConnection(Direction direction) {
        switch (direction) {
            case UP:
                return hasConnectionTop;

            case LEFT:
                return hasConnectionLeft;

            case RIGHT:
                return hasConnectionRight;

            case DOWN:
                return hasConnectionBottom;
            default:
                return false;
        }
    }

    public boolean isHasNoConnection() {
        if (upPoint == null && leftPoint == null && rightPoint == null && downPoint == null) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "[" + pointX + "][" + pointY + "]";
    }
}
