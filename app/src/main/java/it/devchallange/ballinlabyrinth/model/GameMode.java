package it.devchallange.ballinlabyrinth.model;

public enum GameMode {
    // "зі звичайним (вхід і вихід на знаходятся в верхньому лівому і правому нижньому кутах відповідно)"
    STANDARD,

    // з випадковим входом і виходом
    RANDOM
}
