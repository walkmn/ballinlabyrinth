package it.devchallange.ballinlabyrinth.model;

/**
 *
 * Storage all points if labyrinth, ball position (in pixel, or in index) and finish point position.
 *
 */
public class GameDataModel {

    private LabyrinthPoint[][] labyrinthPoints;

    // ball position in pixels
    private int circleX = -1;
    private int circleY = -1;

    // ball position (from 0 to length of labyrinthPoints array)
    private int circlePosX = -1;
    private int circlePosY = -1;

    private int finishPosX = 0;
    private int finishPosY = 0;

    public GameDataModel(LabyrinthPoint[][] labyrinthPoints) {
        this.labyrinthPoints = labyrinthPoints;
    }

    public GameDataModel setFinishPosition(int finishPosX, int finishPosY) {
        this.finishPosX = finishPosX;
        this.finishPosY = finishPosY;
        return this;
    }

    public GameDataModel setCirclePosition(int circlePosX, int circlePosY) {
        this.circlePosX = circlePosX;
        this.circlePosY = circlePosY;
        return this;
    }

    public GameDataModel setCircleCoordinates(int circleX, int circleY) {
        this.circleX = circleX;
        this.circleY = circleY;
        return this;
    }

    public LabyrinthPoint[][] getLabyrinthPoints() {
        return labyrinthPoints;
    }

    public boolean isCircleHasCoordinates() {
        if (circleX != -1 && circleY != -1) {
            return true;
        }
        return false;
    }

    public int getCircleX() {
        return circleX;
    }

    public int getCircleY() {
        return circleY;
    }

    public int getCirclePosX() {
        return circlePosX;
    }

    public int getCirclePosY() {
        return circlePosY;
    }

    public int getFinishPosX() {
        return finishPosX;
    }

    public int getFinishPosY() {
        return finishPosY;
    }
}
