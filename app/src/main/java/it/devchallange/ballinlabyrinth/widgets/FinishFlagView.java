package it.devchallange.ballinlabyrinth.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import it.devchallange.ballinlabyrinth.R;
import it.devchallange.ballinlabyrinth.model.LabyrinthPoint;

/**
 * Drawing flag in necessary position. That all.
 */
public class FinishFlagView extends View {

    // init position
    private int xPos = 0;
    private int yPos = 0;

    private boolean hasCalculatedDistances = false;

    private int offsetX;
    private int offsetY;

    private int offsetHalfX;
    private int offsetHalfY;

    private int flagWidth;
    private int flagHeight;

    // for calculating size (distances) of flag
    private LabyrinthPoint[][] labyrinthPoints;
    private Bitmap flagBitmap;

    private float left;
    private float top;

    public FinishFlagView(Context context) {
        super(context);
        init();
    }

    public FinishFlagView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FinishFlagView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    public void setLabyrinthPoints(LabyrinthPoint[][] labyrinthPoints) {
        this.labyrinthPoints = labyrinthPoints;
        hasCalculatedDistances = false;
        invalidate();
    }

    private void init() {
        flagBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ico_flag_finish);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (!hasCalculatedDistances) {
            calculateDistances();

            float boxCenterX = (offsetX * (xPos + 1));
            float boxCenterY = (offsetY * (yPos + 1));

            left = boxCenterX - offsetHalfX - (flagWidth / 2);
            top = boxCenterY - offsetHalfY - (flagHeight / 2);

        }

        Paint myPaint = new Paint();
        myPaint.setColor(Color.WHITE);
        myPaint.setStyle(Paint.Style.FILL);
        canvas.drawBitmap(flagBitmap, left, top, myPaint);
    }

    private void calculateDistances() {
        offsetX = getWidth() / labyrinthPoints[0].length;
        offsetY = getHeight() / labyrinthPoints.length;

        offsetHalfX = offsetX / 2;
        offsetHalfY = offsetY / 2;

        flagWidth = (int) (offsetX * 0.7);
        flagHeight = (int) (offsetY * 0.7);

        flagBitmap = Bitmap.createScaledBitmap(flagBitmap, flagWidth, flagHeight, true);
    }

    public void setPosition(int finishPosX, int finishPosY) {
        xPos = finishPosX;
        yPos = finishPosY;
        invalidate();
    }
}
