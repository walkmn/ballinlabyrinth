package it.devchallange.ballinlabyrinth.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import it.devchallange.ballinlabyrinth.model.Direction;
import it.devchallange.ballinlabyrinth.model.LabyrinthPoint;

public class LabyrinthFieldView extends View {

    private LabyrinthPoint[][] labyrinthPoints = new LabyrinthPoint[3][3];

    private boolean hasCalculatedDistances = false;

    private int offsetX;
    private int offsetY;

    private int offsetHalfX;
    private int offsetHalfY;

    private int boxPaddingX;
    private int boxPaddingY;

    public LabyrinthFieldView(Context context) {
        super(context);
        init();
    }

    public LabyrinthFieldView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LabyrinthFieldView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LabyrinthFieldView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setBackgroundColor(Color.BLACK);
    }

    public void setLabyrinthPoints(LabyrinthPoint[][] labyrinthPoints) {
            this.labyrinthPoints = labyrinthPoints;
            hasCalculatedDistances = false;
            invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (!hasCalculatedDistances) {
            calculateDistances();
        }

        for (int row = 0; row < labyrinthPoints.length; row++) {
            for (int col = 0; col < labyrinthPoints[row].length; col++) {

                LabyrinthPoint currentPoint = labyrinthPoints[row][col];

                float boxCenterX = (offsetX * (col + 1)) - offsetHalfX;
                float boxCenterY = (offsetY * (row + 1)) - offsetHalfY;

                float left = boxCenterX - offsetHalfX;
                float top = boxCenterY - offsetHalfY;
                float right = boxCenterX + offsetHalfX + 1;
                float bottom = boxCenterY + offsetHalfY + 1;

                if (!currentPoint.isHasConnection(Direction.LEFT)) {
                    left = left + boxPaddingX;
                }

                if (!currentPoint.isHasConnection(Direction.UP)) {
                    top = top + boxPaddingY;
                }
                if (!currentPoint.isHasConnection(Direction.RIGHT)) {
                    right = right - boxPaddingX;
                }
                if (!currentPoint.isHasConnection(Direction.DOWN)) {
                    bottom = bottom - boxPaddingY;
                }

                Paint myPaint = new Paint();
                myPaint.setColor(Color.WHITE);
                myPaint.setStyle(Paint.Style.FILL);
                canvas.drawRect(left, top, right, bottom, myPaint);

            }
        }
    }

    private void calculateDistances() {
        offsetX = getWidth() / labyrinthPoints[0].length;
        offsetY = getHeight() / labyrinthPoints.length;

        offsetHalfX = offsetX / 2;
        offsetHalfY = offsetY / 2;

        boxPaddingX = (int) (offsetX * 0.1);
        boxPaddingY = (int) (offsetY * 0.1);
    }
}
