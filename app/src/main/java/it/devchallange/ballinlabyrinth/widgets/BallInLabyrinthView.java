package it.devchallange.ballinlabyrinth.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import it.devchallange.ballinlabyrinth.model.Direction;
import it.devchallange.ballinlabyrinth.model.GameDataModel;
import it.devchallange.ballinlabyrinth.model.LabyrinthPoint;

/**
 *  BallInLabyrinth custom view. Consist: ball view, labyrinth field view and finish flag view.
 */
public class BallInLabyrinthView extends FrameLayout implements CircleView.BallListener {

    private CircleView circleView;
    private FinishFlagView finishFlagView;
    private LabyrinthFieldView labyrinthFieldView;

    @Nullable
    private GameViewListener gameViewListener;
    private int ballCol;
    private int ballRow;
    private boolean enabledAutoPlay = false;
    private List<LabyrinthPoint> autoPlayWay = new ArrayList<>();

    private LabyrinthPoint finishPoint;

    public void setGameViewListener(GameViewListener gameViewListener) {
        this.gameViewListener = gameViewListener;
    }

    // data
    private GameDataModel gameDataModel;
    private LabyrinthPoint[][] labyrinthPoints;

    public BallInLabyrinthView(Context context) {
        super(context);
        init(context);
    }

    public BallInLabyrinthView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BallInLabyrinthView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BallInLabyrinthView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {

        circleView = new CircleView(context);
        finishFlagView = new FinishFlagView(context);
        labyrinthFieldView = new LabyrinthFieldView(context);

        circleView.setBallListener(this);

        addView(labyrinthFieldView);
        addView(finishFlagView);
        addView(circleView);
    }

    public void moveCircle(Direction direction) {
        circleView.moveCircle(direction);
    }

    public void moveCircle(float axisX, float axisY) {
        circleView.moveCircle(axisX, axisY);
    }

    public void stopBall() {
        circleView.moveCircle(null);
    }

    public void updateFrame() {
        circleView.invalidate();
    }

    public void setGameDataModel(GameDataModel gameModel) {
        this.gameDataModel = gameModel;
        this.labyrinthPoints = gameModel.getLabyrinthPoints();
        finishPoint = labyrinthPoints[gameDataModel.getFinishPosY()][gameDataModel.getFinishPosX()];

        labyrinthFieldView.setLabyrinthPoints(labyrinthPoints);
        circleView.setLabyrinthPoints(labyrinthPoints);
        finishFlagView.setLabyrinthPoints(labyrinthPoints);

        if (gameModel.isCircleHasCoordinates()) {
            circleView.setCoordinates(gameModel.getCircleX(), gameModel.getCircleY());
        } else {
            circleView.setPosition(gameModel.getCirclePosX(), gameModel.getCirclePosY());
        }

        finishFlagView.setPosition(gameModel.getFinishPosX(), gameModel.getFinishPosY());
    }

    public int getBallXCoordinate() {
        return circleView.getCircleX();
    }

    public int getBallYCoordinate() {
        return circleView.getCircleY();
    }

    @Override
    public void onHasNewPosition(int col, int row) {
        ballCol = col;
        ballRow = row;
        if (col == gameDataModel.getFinishPosX() && row == gameDataModel.getFinishPosY()) {
            // than - finish
            if (gameViewListener != null) {
                gameViewListener.onFinished();
            }
        }

        // auto play
        if (enabledAutoPlay) {
            if (autoPlayWay.size() <= 1) {
                autoPlay(false);
                return;
            }
            LabyrinthPoint nextWayPoint = autoPlayWay.get(1);
            circleView.moveCircleToPoint(nextWayPoint);
            autoPlayWay.remove(nextWayPoint);
        }
    }

    public void autoPlay(boolean enable) {
        stopBall();
        enabledAutoPlay = enable;

        if (enabledAutoPlay) {

            // some data
            LabyrinthPoint currentPoint = labyrinthPoints[ballRow][ballCol];

            Log.d("currentPoint", currentPoint.toString());
            Log.d("finishPoint", finishPoint.toString());

            autoPlayWay = currentPoint.findWayToPoint(currentPoint, finishPoint);
            for (int i = 0; i < autoPlayWay.size(); i++) {
                LabyrinthPoint currentPointOfWay = autoPlayWay.get(i);
                Log.d("currentPointOfWay", currentPointOfWay.toString());
            }

            LabyrinthPoint nextWayPoint = autoPlayWay.get(1);
            circleView.moveCircleToPoint(nextWayPoint);
            autoPlayWay.remove(nextWayPoint);
        }
    }

    public interface GameViewListener {
        void onFinished();
    }
}
