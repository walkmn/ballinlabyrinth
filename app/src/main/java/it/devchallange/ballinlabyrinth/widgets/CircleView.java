package it.devchallange.ballinlabyrinth.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import it.devchallange.ballinlabyrinth.model.Direction;
import it.devchallange.ballinlabyrinth.model.LabyrinthPoint;

/**
 * The better name is - Ball View, but something goes wrong.
 *
 * Drawing "ball" and moving its by some rules.
 */
public class CircleView extends View {

    private static final int SPEED_MIN = 1;
    private int SPEED_MAX_X = 5;
    private int SPEED_MAX_Y = 5;

    private LabyrinthPoint[][] labyrinthPoints = new LabyrinthPoint[3][3];

    private boolean hasCalculatedDistances = false;

    private int circleX = 0;
    private int circleY = 0;

    private int circleRadius;
    private int offsetX;
    private int offsetY;
    private int boxPaddingX;
    private int boxPaddingY;
    private int offsetHalfX;
    private int offsetHalfY;
    private int boxHalfWidth;
    private int boxHalfHeight;

    // accelerometer
    private static final float VALUE_FOR_IGNORE_ACCELEROMETER = 0.3f;
    private float sensorAxisX = 0;
    private float sensorAxisY = 0;

    private int circleBorderX;
    private int circleBorderY;

    // init position
    private int circleXPos = 0;
    private int circleYPos = 0;

    // current position
    private int currentCircleCol = 0;
    private int currentCircleRow = 0;

    @Nullable
    private BallListener ballListener;

    int getCircleX() {
        return circleX;
    }

    int getCircleY() {
        return circleY;
    }

    public CircleView(Context context) {
        super(context);
        init();
    }

    public CircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {

        // test
        for (int row = 0; row < labyrinthPoints.length; row++) {
            for (int col = 0; col < labyrinthPoints[row].length; col++) {
                labyrinthPoints[row][col] = new LabyrinthPoint(row, col);
            }
        }
    }

    public void moveCircleToPoint(LabyrinthPoint toPoint) {
        if (toPoint == null) {
            moveCircle(null);
            return;
        }
        if (currentCircleRow != toPoint.getX()) {
            if (currentCircleRow < toPoint.getX()) {
                moveCircle(Direction.DOWN);
            } else {
                moveCircle(Direction.UP);
            }
            return;
        }
        if (currentCircleCol != toPoint.getY()) {
            if (currentCircleCol > toPoint.getY()) {
                moveCircle(Direction.LEFT);
            } else {
                moveCircle(Direction.RIGHT);
            }
        }
    }

    // sensorAxisX and sensorAxisY from sensor
    public void moveCircle(float axisX, float axisY) {
        this.sensorAxisX = axisX;
        this.sensorAxisY = axisY;
    }

    public void moveCircle(Direction direction) {

        if (direction == null) {
            moveCircle(0f, 0f);
            return;
        }

        switch (direction) {
            case UP:
                moveCircle(0f, -5f);
                break;
            case DOWN:
                moveCircle(0f, 5f);
                break;
            case LEFT:
                moveCircle(5f, 0f);
                break;
            case RIGHT:
                moveCircle(-5f, 0f);
                break;
        }
    }

    public void setLabyrinthPoints(LabyrinthPoint[][] labyrinthPoints) {
        this.labyrinthPoints = labyrinthPoints;
        hasCalculatedDistances = false;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (!hasCalculatedDistances) {
            calculateDistances();
        }

        if (sensorAxisX != 0) {

            if (Math.abs(sensorAxisX) > VALUE_FOR_IGNORE_ACCELEROMETER) {

                if (circleBorderY == 0) {
                    circleBorderY = circleY;
                }

                float speed = Math.abs(sensorAxisX) / 5;
                int speedX = (int) (SPEED_MIN + (speed * SPEED_MAX_X));

                // move left
                if (sensorAxisX > 0) {

                    circleBorderX = circleX - circleRadius;

                    int col = getCol(circleBorderX);
                    int row = getRow(circleBorderY);

                    LabyrinthPoint currentPoint = labyrinthPoints[row][col];

                    float boxCenterX = (offsetX * (col + 1)) - offsetHalfX;
                    float boxCenterY = (offsetY * (row + 1)) - offsetHalfY;

                    float left = (boxCenterX - boxHalfWidth);
                    float top = (boxCenterY - boxHalfHeight);
                    float bottom = (boxCenterY + boxHalfHeight);

                    if (isCanMoveLeft(currentPoint, left, top, bottom)) {
                        circleX = circleX - speedX;
                        checkPosition();
                    }

                }
                // move right
                else {

                    circleBorderX = circleX + circleRadius;

                    int col = getCol(circleBorderX);
                    int row = getRow(circleBorderY);

                    LabyrinthPoint currentPoint = labyrinthPoints[row][col];

                    float boxCenterX = (offsetX * (col + 1)) - offsetHalfX;
                    float boxCenterY = (offsetY * (row + 1)) - offsetHalfY;

                    float top = (boxCenterY - boxHalfHeight);
                    float right = (boxCenterX + boxHalfWidth);
                    float bottom = (boxCenterY + boxHalfHeight);

                    if (isCanMoveRight(currentPoint, right, top, bottom)) {
                        circleX = circleX + speedX;
                        checkPosition();
                    }
                }
            }
        }
        if (sensorAxisY != 0) {

            if (Math.abs(sensorAxisY) > VALUE_FOR_IGNORE_ACCELEROMETER) {

                if (circleBorderX == 0) {
                    circleBorderX = circleX;
                }

                float speed = Math.abs(sensorAxisY) / 5;
                int speedY = (int) (SPEED_MIN + (speed * SPEED_MAX_Y));

                // move down
                if (sensorAxisY > 0) {

                    circleBorderY = circleY + circleRadius;

                    int col = getCol(circleBorderX);
                    int row = getRow(circleBorderY);

                    LabyrinthPoint currentPoint = labyrinthPoints[row][col];

                    float boxCenterX = (offsetX * (col + 1)) - offsetHalfX;
                    float boxCenterY = (offsetY * (row + 1)) - offsetHalfY;

                    float left = (boxCenterX - boxHalfWidth);
                    float right = (boxCenterX + boxHalfWidth);
                    float bottom = (boxCenterY + boxHalfHeight);

                    if (isCanMoveDown(currentPoint, bottom, right, left)) {
                        circleY = circleY + speedY;
                        checkPosition();
                    }
                }
                // move up
                else {

                    circleBorderY = circleY - circleRadius;

                    int col = getCol(circleBorderX);
                    int row = getRow(circleBorderY);

                    LabyrinthPoint currentPoint = labyrinthPoints[row][col];

                    float boxCenterX = (offsetX * (col + 1)) - offsetHalfX;
                    float boxCenterY = (offsetY * (row + 1)) - offsetHalfY;

                    float left = (boxCenterX - boxHalfWidth);
                    float top = (boxCenterY - boxHalfHeight);
                    float right = (boxCenterX + boxHalfWidth);

                    if (isCanMoveUp(currentPoint, top, right, left)) {
                        circleY = circleY - speedY;
                        checkPosition();
                    }
                }

            }
        }

        Paint myPaint = new Paint();
        myPaint.setColor(Color.RED);
        myPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(circleX, circleY, circleRadius, myPaint);
    }

    private void checkPosition() {
        if (ballListener != null) {
            int colLeft = getCol(circleX - circleRadius);
            int colRight = getCol(circleX + circleRadius);
            int rowTop = getRow(circleY - circleRadius);
            int rowBottom = getRow(circleY + circleRadius);

            if (colLeft != colRight) {
                return;
            }
            if (rowTop != rowBottom) {
                return;
            }

            if (colLeft != currentCircleCol || rowTop != currentCircleRow) {
                currentCircleCol = colLeft;
                currentCircleRow = rowTop;
                ballListener.onHasNewPosition(currentCircleCol, currentCircleRow);
            }
        }
    }

    private boolean isCanMoveRight(LabyrinthPoint point, float rightBorder, float topBorder, float bottomBorder) {
        if (!point.isHasConnection(Direction.RIGHT)) {
            rightBorder = rightBorder - boxPaddingX;

            if (circleX + circleRadius >= rightBorder) {
                return false;
            }
        }
        if (!point.isHasConnection(Direction.UP)) {
            if (circleY - circleRadius <= topBorder) {
                return false;
            }
        }
        if (!point.isHasConnection(Direction.DOWN)) {
            if (circleY + circleRadius >= bottomBorder) {
                return false;
            }
        }
        return true;
    }

    private boolean isCanMoveLeft(LabyrinthPoint point, float leftBorder, float topBorder, float bottomBorder) {
        if (!point.isHasConnection(Direction.LEFT)) {
            leftBorder = leftBorder + boxPaddingX;

            if (circleX - circleRadius <= leftBorder) {
                return false;
            }
        }
        if (!point.isHasConnection(Direction.UP)) {
            if (circleY - circleRadius <= topBorder) {
                return false;
            }
        }
        if (!point.isHasConnection(Direction.DOWN)) {
            if (circleY + circleRadius >= bottomBorder) {
                return false;
            }
        }
        return true;
    }

    private boolean isCanMoveUp(LabyrinthPoint point, float topBorder, float rightBorder, float leftBorder) {
        if (!point.isHasConnection(Direction.UP)) {
            topBorder = topBorder + boxPaddingX;

            if (circleY - circleRadius <= topBorder) {
                return false;
            }
        }
        if (!point.isHasConnection(Direction.RIGHT)) {
            if (circleX + circleRadius >= rightBorder) {
                return false;
            }
        }
        if (!point.isHasConnection(Direction.LEFT)) {
            if (circleX - circleRadius <= leftBorder) {
                return false;
            }
        }
        return true;
    }

    private boolean isCanMoveDown(LabyrinthPoint point, float bottomBorder, float rightBorder, float leftBorder) {
        if (!point.isHasConnection(Direction.DOWN)) {
            bottomBorder = bottomBorder - boxPaddingX;

            if (circleY + circleRadius >= bottomBorder) {
                return false;
            }
        }
        if (!point.isHasConnection(Direction.RIGHT)) {
            if (circleX + circleRadius >= rightBorder) {
                return false;
            }
        }
        if (!point.isHasConnection(Direction.LEFT)) {
            if (circleX - circleRadius <= leftBorder) {
                return false;
            }
        }
        return true;
    }

    private int getCol(int x) {
        return x / offsetX;
    }

    private int getRow(int y) {
        return y / offsetY;
    }

    private void calculateDistances() {
        offsetX = getWidth() / labyrinthPoints[0].length;
        offsetY = getHeight() / labyrinthPoints.length;

        offsetHalfX = offsetX / 2;
        offsetHalfY = offsetY / 2;

        boxHalfWidth = (offsetX / 2) + 2;
        boxHalfHeight = (offsetY / 2) + 2;

        boxPaddingX = (int) (offsetX * 0.1);
        boxPaddingY = (int) (offsetY * 0.1);

        circleRadius = (boxHalfHeight < boxHalfWidth) ? boxHalfHeight : boxHalfWidth;
        circleRadius /= 3;

        if (circleX == 0 && circleY == 0) {
            circleX = offsetHalfX + (offsetX  * (circleXPos));
            circleY = offsetHalfY + (offsetY  * (circleYPos));
        }

        SPEED_MAX_X = offsetX / 50;
        SPEED_MAX_Y = offsetY / 50;

        hasCalculatedDistances = true;

        checkPosition();
    }

    public void setPosition(int circleXPos, int circleYPos) {
        this.circleXPos = circleXPos;
        this.circleYPos = circleYPos;
        hasCalculatedDistances = false;
        invalidate();
    }

    public void setCoordinates(int circleX, int circleY) {
        this.circleX = circleX;
        this.circleY = circleY;
        hasCalculatedDistances = false;
        invalidate();
    }

    public void setBallListener(@Nullable BallListener ballListener) {
        this.ballListener = ballListener;
    }

    public interface BallListener {
        // position, not coordinate
        void onHasNewPosition(int col, int row);
    }
}
