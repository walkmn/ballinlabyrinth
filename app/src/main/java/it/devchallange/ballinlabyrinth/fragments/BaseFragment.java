package it.devchallange.ballinlabyrinth.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseFragment extends Fragment {

    public Activity mActivity;

    protected View rootView;

    protected @LayoutRes int layoutResSaved = 0;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    protected abstract @LayoutRes int getLayoutResource();

    protected View findViewById(@IdRes int resourceId) {
        return rootView.findViewById(resourceId);
    }

    protected void onInit(View rootView) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getLayoutResource() != 0) {
            layoutResSaved = getLayoutResource();
        }
        rootView = inflater.inflate(layoutResSaved, container, false);

        onInit(rootView);

        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            layoutResSaved = savedInstanceState.getInt("layoutResSaved");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("layoutResSaved", layoutResSaved);
    }
}
