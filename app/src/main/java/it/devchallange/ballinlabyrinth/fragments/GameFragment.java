package it.devchallange.ballinlabyrinth.fragments;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import java.util.Timer;
import java.util.TimerTask;

import it.devchallange.ballinlabyrinth.R;
import it.devchallange.ballinlabyrinth.model.Direction;
import it.devchallange.ballinlabyrinth.model.GameDataModel;
import it.devchallange.ballinlabyrinth.widgets.BallInLabyrinthView;

/**
 * Fragment for play game.
 */
public class GameFragment extends BaseFragment implements SensorEventListener, BallInLabyrinthView.GameViewListener {

    private Switch switchAutoPlay;
    private BallInLabyrinthView ballInLabyrinthView;

    private View llAccelerometerControl, llKeyControl;

    private static final int TIME_FOR_UPDATE_FRAME = 10;

    private Direction lastPressedDirection;

    // sensor
    private SensorManager sensorManager = null;
    private boolean isEnabledSensor = false;
    private float currentAxisX = 0, currentAxisY = 0;
    private float axisXOffset = 0, axisYOffset = 0;

    private Timer mTimer;

    @Nullable
    private GameListener gameListener;
    private GameDataModel gameDataModel;
    private boolean isGameIsFinished = false;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_game;
    }

    @Override
    protected void onInit(View rootView) {
        super.onInit(rootView);

        if (gameDataModel == null) {
            throw new NullPointerException("Please pass GameDataModel `setGameDataModel(...);` for GameFragment. It is base object for showing labyrinth and ball.");
        }

        isGameIsFinished = false;

        final Switch switchChangeControlMode = (Switch) findViewById(R.id.switchChangeControlMode);
        llKeyControl = findViewById(R.id.llKeyControl);
        llAccelerometerControl = findViewById(R.id.llAccelerometerControl);
        switchChangeControlMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    llKeyControl.setVisibility(View.GONE);
                    llAccelerometerControl.setVisibility(View.VISIBLE);
                } else {
                    llKeyControl.setVisibility(View.VISIBLE);
                    llAccelerometerControl.setVisibility(View.GONE);
                }
                ballInLabyrinthView.stopBall();
                isEnabledSensor = checked;
            }
        });

        ballInLabyrinthView = (BallInLabyrinthView) findViewById(R.id.circleInLabyrinthView);
        ballInLabyrinthView.setGameDataModel(gameDataModel);
        ballInLabyrinthView.setGameViewListener(this);

        Button btnCalibrate = (Button) findViewById(R.id.btn_calibrate);
        btnCalibrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                axisXOffset = currentAxisX;
                axisYOffset = currentAxisY;
            }
        });

        // control buttons
        Button btnLeft = (Button) findViewById(R.id.btn_left);
        Button btnRight = (Button) findViewById(R.id.btn_right);
        Button btnUp = (Button) findViewById(R.id.btn_up);
        Button btnDown = (Button) findViewById(R.id.btn_down);

        setupControlOnTouchListener(btnLeft, Direction.LEFT);
        setupControlOnTouchListener(btnRight, Direction.RIGHT);
        setupControlOnTouchListener(btnUp, Direction.UP);
        setupControlOnTouchListener(btnDown, Direction.DOWN);

        // auto play mode
        switchAutoPlay = (Switch) findViewById(R.id.switch_auto_play);
        switchAutoPlay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    isEnabledSensor = false;
                    switchChangeControlMode.setChecked(false);

                    findViewById(R.id.ll_control_section).setVisibility(View.GONE);
                } else {
                    findViewById(R.id.ll_control_section).setVisibility(View.VISIBLE);
                }
                ballInLabyrinthView.autoPlay(checked);
            }
        });

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ballInLabyrinthView.updateFrame();
                    }
                });
            }
        }, 0, TIME_FOR_UPDATE_FRAME);

        sensorManager = (SensorManager) mActivity.getSystemService(Activity.SENSOR_SERVICE);
    }

    private void setupControlOnTouchListener(Button button, final Direction direction) {
        button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    lastPressedDirection = direction;
                    ballInLabyrinthView.moveCircle(direction);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (lastPressedDirection != null && lastPressedDirection == direction) {
                        ballInLabyrinthView.stopBall();
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (switchAutoPlay.isChecked()) {
            switchAutoPlay.setChecked(false);
        }

        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();
        if (gameListener != null && !isGameIsFinished) {
            gameDataModel.setCircleCoordinates(
                    ballInLabyrinthView.getBallXCoordinate(),
                    ballInLabyrinthView.getBallYCoordinate()
            );
            gameListener.onGamePaused(gameDataModel);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        synchronized (this) {
            switch (sensorEvent.sensor.getType()){
                case Sensor.TYPE_ACCELEROMETER:

                    currentAxisX = sensorEvent.values[0];
                    currentAxisY = sensorEvent.values[1];

                    if (isEnabledSensor) {
                        ballInLabyrinthView.moveCircle(
                                currentAxisX - (axisXOffset),
                                currentAxisY - (axisYOffset)
                        );
                    }

                    break;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void setGameDataModel(GameDataModel gameDataModel) {
        this.gameDataModel = gameDataModel;
    }

    public void setGameListener(GameListener gameListener) {
        this.gameListener = gameListener;
    }

    @Override
    public void onFinished() {
        mTimer.cancel();
        if (gameListener != null) {
            gameListener.onGameFinished();
            // this game cannot to continue
            gameListener.onGamePaused(null);
            isGameIsFinished = true;
        }
    }

    public interface GameListener {
        void onGameFinished();
        void onGamePaused(GameDataModel gameDataModel);
    }
}
