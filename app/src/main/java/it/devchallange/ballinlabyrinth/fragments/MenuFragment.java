package it.devchallange.ballinlabyrinth.fragments;

import android.support.annotation.Nullable;
import android.view.View;

import it.devchallange.ballinlabyrinth.R;

/**
 * Main game menu fragment.
 */
public class MenuFragment extends BaseFragment {


    @Nullable
    private GameMenuListener gameMenuListener;

    public void setGameMenuListener(@Nullable GameMenuListener gameMenuListener) {
        this.gameMenuListener = gameMenuListener;
    }

    @Override
    protected void onInit(View rootView) {
        super.onInit(rootView);

        findViewById(R.id.btnNewGame).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (gameMenuListener != null) {
                    gameMenuListener.onNewGameClicked();
                }
            }
        });

        findViewById(R.id.btnContinueGame).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (gameMenuListener != null) {
                    gameMenuListener.onContinueGameClicked();
                }
            }
        });

        findViewById(R.id.btnSaveGame).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (gameMenuListener != null) {
                    gameMenuListener.onSaveGameClicked();
                }
            }
        });

        findViewById(R.id.btnLoadGame).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (gameMenuListener != null) {
                    gameMenuListener.onLoadGameClicked();
                }
            }
        });
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_menu;
    }

    public interface GameMenuListener {
        void onNewGameClicked();
        void onContinueGameClicked();
        void onSaveGameClicked();
        void onLoadGameClicked();
    }
}
